let today = new Date();
let currentMonth = today.getMonth();
let currentYear = today.getFullYear();
let selectYear = document.getElementById("year");
let selectMonth = document.getElementById("month");
let profile = document.getElementById("profile-button");
let apartments = document.getElementById("apartment-button");

let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

let monthAndYear = document.getElementById("monthAndYear");

let calendar_data = null

fetch('http://0.0.0.0:8001/api/calendar/', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
          }
    })
    .then(response => response.json())
    .then(data => {
        calendar_data = data
        return data
}).then(() => {
    showCalendar(currentMonth, currentYear)
})

function next() {
    currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
    currentMonth = (currentMonth + 1) % 12;
    showCalendar(currentMonth, currentYear);
}

function previous() {
    currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
    currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
    showCalendar(currentMonth, currentYear);
}

function jump() {
    currentYear = parseInt(selectYear.value);
    currentMonth = parseInt(selectMonth.value);
    showCalendar(currentMonth, currentYear);
}

function showCalendar(month, year) {

    let firstDay = (new Date(year, month)).getDay();
    let daysInMonth = 32 - new Date(year, month, 32).getDate();

    let tbl = document.getElementById("calendar-body"); // body of the calendar

    // clearing all previous cells
    tbl.innerHTML = "";

    // filing data about month and in the page via DOM.
    monthAndYear.innerHTML = months[month] + " " + year;
    selectYear.value = year;
    selectMonth.value = month;

    // creating all cells
    let date = 1;
    for (let i = 0; i < 6; i++) {
        // creates a table row
        let row = document.createElement("tr");

        //creating individual cells, filing them up with data.
        for (let j = 0; j < 7; j++) {
            if (i === 0 && j < firstDay) {
                let cell = document.createElement("td");
                let cellText = document.createTextNode("");
                cell.appendChild(cellText);
                row.appendChild(cell);
            }
            else if (date > daysInMonth) {
                break;
            }

            else {
                let cell = document.createElement("td");
                let cellText = document.createTextNode(date);

                cell.appendChild(cellText);
                
                var zerofilled_date = ('00'+date).slice(-2);
                var zerofilled_month = ('00'+(parseInt(month, 10) + 1)).slice(-2)
                entries = calendar_data.calendar[zerofilled_month + '-' + zerofilled_date + '-' + year]

                if (entries) {
                    entries.forEach(function (entry) {
                        
                        cell.appendChild(document.createElement('br'))
                        let apartmentText = document.createTextNode(entry.apartment.name + ': ' + entry.status)
                        cell.appendChild(apartmentText)
                    })
                };

                cleaning_day = calendar_data.to_clean[zerofilled_month + '-' + zerofilled_date + '-' + year]
                if (cleaning_day) {
                    cell.appendChild(document.createElement('br'))
                    cell.appendChild(document.createTextNode('CLEANING DAY:'))
                    cleaning_day.forEach(function (apt) {

                        cell.appendChild(document.createElement('br'))
                        cell.appendChild(document.createTextNode(apt.name))
                        cell.classList.add("bg-primary", "text-white")
                    })
                }
                
                if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
                    cell.classList.add("bg-info");
                } // color today's date
                
                row.appendChild(cell);
                date++;
            }


        }

        tbl.appendChild(row); // appending each row into calendar body.
    }

}

profile.addEventListener("click", function() {
    location.href="profile.html";
})

apartments.addEventListener("click", function() {
    location.href="apartments.html";
})