const loginForm = document.getElementById("login-form");
const loginErrorMsg = document.getElementById("login-error-msg");
const registerButton = document.getElementById("register-form-submit");

registerButton.addEventListener("click", (e) => {
    e.preventDefault();

    const username = loginForm.username.value;
    const password = loginForm.password.value;
    const repeat_password = loginForm.repeat.value;

    fetch('http://0.0.0.0:8001/api/users/', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({username: username, password: password, repeat_password: repeat_password})})
    .then(response => response.json())
    .then(data => {
        if (data.status === "success") {
            alert("You have successfully registered.");
            location.href="index.html";
        }
        else {
            let field = Object.keys(data.errors)
            
            loginErrorMsg.innerText = field + ': ' + data.errors[field]

            loginErrorMsg.style.opacity = 1;
        }
    })
})


