const updateForm = document.getElementById("update-form");
const username = document.getElementById("username-field")
const first_name = document.getElementById("first-name-field")
const last_name = document.getElementById("last-name-field")
const e_mail = document.getElementById("email-field")
const updateButton = document.getElementById("update-form-submit")
const updateErrorMsg = document.getElementById("update-error-msg");
const calendarButton = document.getElementById("calendar-button");
const apartmentButton = document.getElementById("apartment-button");


fetch('http://0.0.0.0:8001/api/users/', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
          }
    })
    .then(response => response.json())
    .then(data => {
        user_data = data

        username.value = user_data.user.username
        first_name.value = user_data.user.first_name
        last_name.value = user_data.user.last_name
        e_mail.value = user_data.user.email
    })


updateButton.addEventListener("click", (e) => {
    // Prevent the default submission of the form
    e.preventDefault();
    // Get the values input by the user in the form fields
    const username = updateForm.username.value;
    const firstname = updateForm.firstname.value;
    const lastname = updateForm.lastname.value;
    const email = updateForm.email.value;

    fetch('http://0.0.0.0:8001/api/users/', {
        method: 'PATCH',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": "Bearer " + localStorage.getItem('token')
          },
        body: JSON.stringify({username: username, first_name: firstname, last_name: lastname, email: email})})
    .then(response => response.json())
    .then(data => {
        if (data.status === "success") {
            alert("You have successfully updated your data.");
        }
        else {
            let field = Object.keys(data.errors)
            
            updateErrorMsg.innerText = field + ': ' + data.errors[field]

            updateErrorMsg.style.opacity = 1;
        }
    })

})

calendarButton.addEventListener("click", function() {
    location.href="calendar.html";
})

apartmentButton.addEventListener("click", function() {
    location.href="apartments.html";
})