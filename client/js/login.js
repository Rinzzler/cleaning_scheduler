const loginForm = document.getElementById("login-form");
const loginButton = document.getElementById("login-form-submit");
const loginErrorMsg = document.getElementById("login-error-msg");
const registerButton = document.getElementById("register-button");

registerButton.addEventListener("click", function() {
    location.href="register.html";
})

// When the login button is clicked, the following code is executed
loginButton.addEventListener("click", (e) => {
    // Prevent the default submission of the form
    e.preventDefault();
    // Get the values input by the user in the form fields
    const username = loginForm.username.value;
    const password = loginForm.password.value;

    fetch('http://0.0.0.0:8001/api/auth/login/', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({username: username, password: password})})
    .then(response => response.json())
    .then(data => {
        if(data.status === "success"){
            localStorage.setItem('token', data.token.access); // will successfully save to localstorage
            
            alert("You have successfully logged in.");
            location.href="calendar.html";
        }
        else {
        // Otherwise, make the login error message show (change its oppacity)
            loginErrorMsg.style.opacity = 1;
        }
    })
})