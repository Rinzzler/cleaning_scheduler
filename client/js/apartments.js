const apartment_links = document.getElementById("main-holder")
const new_apartment_button = document.getElementById("new-apartment-button")
const calendar_button = document.getElementById("calendar-button");
const profile_button = document.getElementById("profile-button");

calendar_button.addEventListener("click", function() {
    location.href = "calendar.html"
})
profile_button.addEventListener("click", function() {
    location.href = "profile.html"
})

fetch('http://0.0.0.0:8001/api/apartments/', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
          }
    })
    .then(response => response.json())
    .then(data => {
        apartment_data = data
        apartment_data.apartment.forEach(element => {
            apartment = document.createElement("button")
            apartment.innerHTML = element.name
            apartment.value = element.id
            apartment.id = "existing-apartment-button"
            apartment.addEventListener("click", function() {
                location.href="apartment_edit.html?id=" + element.id
            })
            apartment_links.appendChild(apartment)
        }); 
    })

new_apartment_button.addEventListener("click", function() {
    location.href="apartment_edit.html";
})

