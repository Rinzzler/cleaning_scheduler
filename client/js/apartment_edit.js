const updateButton = document.getElementById("update-form-submit");
const updateForm = document.getElementById("update-form");
const updateErrorMsg = document.getElementById("update-error-msg");
const name = document.getElementById("name-field");
const address = document.getElementById("address-field");
const city = document.getElementById("city-field");
const calendar_url = document.getElementById("calendar-url-field");
const calendar_button = document.getElementById("calendar-button");
const profile_button = document.getElementById("profile-button");
const export_button = document.getElementById("export-button");

calendar_button.addEventListener("click", function() {
    location.href = "calendar.html"
})
profile_button.addEventListener("click", function() {
    location.href = "profile.html"
})

let delete_button = document.getElementById("delete-button");
    delete_button.addEventListener("click", function() {
        fetch("http://0.0.0.0:8001/api/apartments/" + urlParams.get("id") + "/", {
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + localStorage.getItem('token')
            },
            })
        .then(function() {
            location.href = "apartments.html"
        })
})

function download(url, filename) {
    fetch(url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": "Bearer " + localStorage.getItem('token')
          },
    }).then(function(t) {
        return t.blob().then((b)=>{
            var a = document.createElement("a");
            a.href = URL.createObjectURL(b);
            a.setAttribute("download", filename);
            a.click();
        }
        );
    });
    }

export_button.addEventListener("click", function() {
    download("http://0.0.0.0:8001/api/apartments/" + urlParams.get("id") + "/calendar/export/", name.value + "_calendar.ics")
})


param = window.location.search;
const urlParams = new URLSearchParams(param);



if (urlParams.get("id")) {
    fetch('http://0.0.0.0:8001/api/apartments/' + urlParams.get("id") + "/", {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": "Bearer " + localStorage.getItem('token')
          },
        })
    .then(response => response.json())
    .then(data => {
        if (data.status === "success") {
            name.value = data.apartment.name;
            address.value = data.apartment.address;
            city.value = data.apartment.city;
        }
    })

    fetch("http://0.0.0.0:8001/api/apartments/" + urlParams.get("id") + "/calendar/", {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": "Bearer " + localStorage.getItem('token')
          },
        })
    .then(response => response.json())
    .then(data => {
        if (data.status === "success") {
            calendar_url.value = data.calendar.calendar_url;
        }
    })
}
else {
    delete_button.style.display = "none";
    export_button.style.display = "none";
}

updateButton.addEventListener("click", (e) => {
    // Prevent the default submission of the form
    e.preventDefault();
    // Get the values input by the user in the form fields
    const name = updateForm.name.value;
    const address = updateForm.address.value;
    const city = updateForm.city.value;
    const calendar_url = updateForm.calendarurl.value;

    if (urlParams.get("id")) {
        fetch('http://0.0.0.0:8001/api/apartments/' + urlParams.get("id") + "/", {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + localStorage.getItem('token')
            },
            body: JSON.stringify({name: name, address: address, city: city})})
        .then(response => response.json())
        .then(data => {
            if (data.status === "success") {
                alert("You have successfully updated your data.");
            }
            else {
                let field = Object.keys(data.errors)
                
                updateErrorMsg.innerText = field + ': ' + data.errors[field]

                updateErrorMsg.style.opacity = 1;
            }
        })
        .then(function() {
            fetch("http://0.0.0.0:8001/api/apartments/" + urlParams.get("id") + "/calendar/", {
                method: "POST",
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + localStorage.getItem('token')
                },
                body: JSON.stringify({calendar_url: calendar_url})
            })
        })
    }
    else {
        let new_apartment_id = null;
        
        fetch("http://0.0.0.0:8001/api/apartments/", {
            method: "POST",
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + localStorage.getItem('token')
            },
            body: JSON.stringify({name: name, address: address, city: city})})
        .then(response => response.json())
        .then(data => {
            if (data.status === "success") {
                alert("You have successfully created your apartment.");
                new_apartment_id = data.apartment.id
            }
            else {
                let field = Object.keys(data.errors)
                
                updateErrorMsg.innerText = field + ': ' + data.errors[field]

                updateErrorMsg.style.opacity = 1;
            }
        })
        .then(function() {
            fetch("http://0.0.0.0:8001/api/apartments/" + new_apartment_id + "/calendar/", {
                method: "POST",
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + localStorage.getItem('token')
                },
                body: JSON.stringify({calendar_url: calendar_url})
            })
        })
        .then(function () {
            location.href = "apartments.html"
        })
    }

})
