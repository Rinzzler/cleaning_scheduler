from django.urls import path

import apartments.views

urlpatterns = [
    path('apartments/', apartments.views.ApartmentCRUD.as_view(),
         name='apartment-create'),
    path('apartments/<int:apartment_id>/', apartments.views.ApartmentCRUD.as_view(),
         name='apartment-rud'),
    path('apartments/<int:apartment_id>/calendar/', apartments.views.CalendarsCRUD.as_view(),
         name='calendar-crud'),
    path('apartments/<int:apartment_id>/calendar/export/',
         apartments.views.CalendarExport.as_view(), name='calendar-export'),
    path('calendar/', apartments.views.CalendarEntries.as_view(),
         name='calendar-entries'),
]
