from rest_framework import serializers

from apartments.models import Apartment, Calendar
from users.serializers import UserSerializer


class ApartmentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Apartment
        fields = '__all__'

    def create(self, validated_data):
        validated_data['user'] = self.context['user']
        return super().create(validated_data)


class CalendarSerializer(serializers.ModelSerializer):
    apartment = ApartmentSerializer(read_only=True)

    class Meta:
        model = Calendar
        fields = '__all__'

    def validate(self, data):
        if data.get('calendar_url'):
            if data.get('calendar'):
                raise serializers.ValidationError(
                    '\'calendar_url\' and \'calendar\' can not be both set.')
        elif not data.get('calendar'):
            raise serializers.ValidationError('Either calendar_url or calendar must be set.')

        return data

    def create(self, validated_data):
        validated_data['apartment'] = self.context['apartment']
        return super().create(validated_data)

    def update(self, instance, validated_data):
        if validated_data.get('calendar_url'):
            validated_data['calendar'] = None
        else:
            validated_data['calendar_url'] = ''
        return super().update(instance, validated_data)
