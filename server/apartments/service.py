from datetime import timedelta

from apartments.models import Apartment


def get_cleaning_schedule(calendar_events):
    apartments_to_clean = set()
    cleaning_schedule = dict()
    for i in sorted(calendar_events.keys()):
        for event in calendar_events[i]:
            if event['status'] == 'exit':
                apartments_to_clean.add(event['apartment']['id'])

        for event in calendar_events[i]:
            if event['status'] == 'entry':
                if apartments_to_clean:
                    cleaning_schedule[i] = {'apartments': list(apartments_to_clean)}
                    apartments_to_clean.clear()

    cleaning_schedule_updated = dict()
    for k in cleaning_schedule.keys():
        apt_list = []
        for v in cleaning_schedule[k].get('apartments'):
            apt_list.append({
                'id': Apartment.objects.get(id=v).id,
                'name': Apartment.objects.get(id=v).name
            })
        cleaning_schedule_updated[k] = apt_list

    return cleaning_schedule_updated


def group_calendar_events(date, calendar, calendar_events, status, filters):
    if filters.get('from') is not None and filters.get('to') is not None:
        if filters.get('from') <= date.strftime('%m-%d-%Y') <= filters.get('to'):
            pass
        else:
            return

    try:
        calendar_events[date].append({
            'apartment': {
                'id': calendar.apartment.id,
                'name': calendar.apartment.name,
            },
            'status': status
        })
    except KeyError:
        calendar_events[date] = [{
            'apartment': {
                'id': calendar.apartment.id,
                'name': calendar.apartment.name,
            },
            'status': status
        }]


def populate_calendar_events(calendar, ics_calendar, calendar_events, filters):
    for event in ics_calendar.events:
        date = event.begin
        group_calendar_events(
            date.strftime('%m-%d-%Y'), calendar, calendar_events, 'entry', filters)

        # the algorithm does not need these days between, avoiding additional calendar work
        for i in range(1, event.duration.days):
            date = event.begin + timedelta(days=i)
            group_calendar_events(
                date.strftime('%m-%d-%Y'), calendar, calendar_events, 'occupied', filters)

        date = date + timedelta(days=1)
        group_calendar_events(
            date.strftime('%m-%d-%Y'), calendar, calendar_events, 'exit', filters)
