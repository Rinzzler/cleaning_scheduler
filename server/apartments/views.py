import os
import tempfile

import requests
from django.conf import settings
from django.http import FileResponse
from ics import Calendar as IcsCalendar
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apartments.decorators import can_access_resource
from apartments.models import Apartment, Calendar
from apartments.serializers import ApartmentSerializer, CalendarSerializer
from apartments.service import populate_calendar_events, get_cleaning_schedule


class ApartmentCRUD(APIView):
    @can_access_resource()
    def get(self, request, apartment_id=None):
        if apartment_id is not None:
            try:
                apartment = Apartment.objects.get(id=apartment_id, user=request.user)
                apartment_serializer = ApartmentSerializer(apartment)
            except Apartment.DoesNotExist:
                return Response({
                    'status': 'error',
                    'errors': 'Apartment with specified id does not exist.'
                }, status=status.HTTP_400_BAD_REQUEST)
        else:
            apartments = Apartment.objects.filter(user=request.user)
            apartment_serializer = ApartmentSerializer(apartments, many=True)
        return Response({
            'status': 'success',
            'apartment': apartment_serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request):
        apartment_serializer = ApartmentSerializer(
            data=request.data, context={'user': request.user})
        if apartment_serializer.is_valid():
            apartment_serializer.save()
            return Response({
                'status': 'success',
                'apartment': apartment_serializer.data
            }, status=status.HTTP_200_OK)
        return Response({
            'status': 'error',
            'errors': apartment_serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)

    @can_access_resource()
    def patch(self, request, apartment_id):
        try:
            apartment = Apartment.objects.get(id=apartment_id, user=request.user)
        except Apartment.DoesNotExist:
            return Response({
                'status': 'error',
                'errors': 'Apartment with specified id does not exist.'
            }, status=status.HTTP_400_BAD_REQUEST)

        apartment_serializer = ApartmentSerializer(apartment, data=request.data, partial=True)
        if apartment_serializer.is_valid():
            apartment_serializer.save()
            return Response({
                'status': 'success',
                'apartment': apartment_serializer.data
            }, status=status.HTTP_200_OK)

        return Response({
            'status': 'error',
            'errors': apartment_serializer.errors
        })

    @can_access_resource()
    def delete(self, request, apartment_id):
        try:
            Apartment.objects.get(id=apartment_id).delete()
        except Apartment.DoesNotExist:
            return Response({
                'status': 'error',
                'errors': 'Apartment with specified id does not exist.'
            })

        return Response({
            'status': 'success'
        }, status=status.HTTP_204_NO_CONTENT)


class CalendarsCRUD(APIView):
    @can_access_resource()
    def get(self, request, apartment_id):
        try:
            calendars = Calendar.objects.get(apartment__id=apartment_id)
        except Calendar.DoesNotExist:
            return Response({
                'status': 'success',
                'calendar': []
            }, status=status.HTTP_204_NO_CONTENT)

        calendar_serializer = CalendarSerializer(calendars)
        return Response({
            'status': 'success',
            'calendar': calendar_serializer.data
        }, status=status.HTTP_200_OK)

    @can_access_resource()
    def post(self, request, apartment_id):
        apartment = Apartment.objects.get(id=apartment_id)
        try:
            calendar = Calendar.objects.get(apartment=apartment)
            calendar_serializer = CalendarSerializer(
                calendar, data=request.data, partial=True)
        except Calendar.DoesNotExist:
            calendar_serializer = CalendarSerializer(
                data=request.data, context={'apartment': apartment})

        if calendar_serializer.is_valid():
            calendar_serializer.save()
            return Response({
                'status': 'success',
                'calendar': calendar_serializer.data
            })
        return Response({
            'status': 'error',
            'errors': calendar_serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)

    @can_access_resource()
    def delete(self, request, apartment_id):
        Calendar.objects.get(apartment__id=apartment_id).delete()

        return Response({
            'status': 'success'
        }, status=status.HTTP_204_NO_CONTENT)


class CalendarEntries(APIView):
    def get(self, request):
        calendar_events = dict()
        calendars = Calendar.objects.filter(apartment__user=request.user)
        for calendar in calendars:
            if calendar.calendar_url:
                ics_calendar = IcsCalendar(requests.get(calendar.calendar_url).text)
                populate_calendar_events(calendar, ics_calendar, calendar_events, request.GET)
            else:
                calendar_file = open(settings.MEDIA_ROOT + '/' + str(calendar.calendar), 'r')
                ics_calendar = IcsCalendar(calendar_file.read())
                populate_calendar_events(calendar, ics_calendar, calendar_events, request.GET)

        cleaning_schedule = get_cleaning_schedule(calendar_events)

        return Response({
            'status': 'success',
            'calendar': calendar_events,
            'to_clean': cleaning_schedule
        }, status=status.HTTP_200_OK)


class CalendarExport(APIView):
    @can_access_resource()
    def get(self, request, apartment_id):
        try:
            calendar = Calendar.objects.get(apartment__id=apartment_id)

            if calendar.calendar_url:
                file = requests.get(calendar.calendar_url).text
                try:
                    tmp = tempfile.NamedTemporaryFile(delete=False)
                    with open(tmp.name, 'w') as fi:
                        fi.write(file)
                    return FileResponse(open(tmp.name, 'rb'), as_attachment=True,
                                        filename=calendar.apartment.name + '_calendar.ics')
                finally:
                    os.remove(tmp.name)

            elif calendar.calendar:
                file = open(settings.MEDIA_ROOT + '/' + str(calendar.calendar), 'rb')
                return FileResponse(file)
        except Calendar.DoesNotExist:
            pass

        return Response()
