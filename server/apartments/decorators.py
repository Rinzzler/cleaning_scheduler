from rest_framework.exceptions import PermissionDenied

from apartments.models import Apartment


def can_access_resource():
    """
        This decorator checks that the user requesting the view is the owner of the resource or a
        superuser.

        :return: view
    """
    def _view_wrapper(view):
        def _arguments_wrapper(method_request, *args, **kwargs):
            request = args[0]

            if not request.user.is_superuser and kwargs.get('apartment_id'):
                apartment = Apartment.objects.filter(id=kwargs.get('apartment_id'))
                if apartment:
                    filtered = apartment.filter(user=request.user)
                    if not filtered.exists():
                        raise PermissionDenied

            return view(method_request, *args, **kwargs)

        return _arguments_wrapper
    return _view_wrapper
