# Generated by Django 3.1.6 on 2021-02-16 14:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apartments', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='apartment',
            options={'verbose_name': 'Apartment', 'verbose_name_plural': 'Apartments'},
        ),
        migrations.AlterModelTable(
            name='apartment',
            table='apartments',
        ),
    ]
