from django.contrib.auth.models import User
from django.db import models


class Apartment(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='apartments', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = 'Apartment'
        verbose_name_plural = 'Apartments'
        db_table = 'apartments'


class Calendar(models.Model):
    id = models.AutoField(primary_key=True)
    # ForeignKey instead of OneToOneField, easier to allow for multiple calendars if necessary
    apartment = models.ForeignKey(Apartment, related_name='calendars', on_delete=models.CASCADE,
                                  unique=True)
    calendar_url = models.CharField(max_length=255, blank=True)
    calendar = models.FileField(upload_to='calendars/', null=True, blank=True)

    class Meta:
        verbose_name = 'Calendar'
        verbose_name_plural = 'Calendars'
        db_table = 'calendars'
