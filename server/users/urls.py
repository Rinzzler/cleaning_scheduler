from django.urls import path
from rest_framework_simplejwt import views as jwt_views

import users.views

urlpatterns = [
    path('auth/login/', users.views.Login.as_view(),
         name='token_obtain_pair'),
    path('auth/token-refresh/', jwt_views.TokenRefreshView.as_view(),
         name='token_refresh'),
    path('auth/token-verify/', jwt_views.TokenVerifyView.as_view(),
         name='token_verify'),
    path('users/', users.views.UserCRUD.as_view(), name='user-crud'),

]
