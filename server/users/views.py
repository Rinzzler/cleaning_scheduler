from django.contrib.auth.models import User
from rest_framework import status, permissions
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

import users.serializers


class Login(TokenObtainPairView):
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if response.status_code == 200:
            user = User.objects.get(username=request.data['username'])
            user_serializer = users.serializers.UserSerializer(user)
            return Response({
                'status': 'success',
                'user': user_serializer.data,
                'token': response.data
            })

        return response


class UserCRUD(APIView):
    serializer = users.serializers.UserSerializer

    def get_permissions(self):
        if self.request.method == 'POST':
            return permissions.AllowAny(),
        return super().get_permissions()

    def get(self, request, *args, **kwargs):
        user_serializer = users.serializers.UserSerializer(request.user)
        return Response({
            'status': 'success',
            'user': user_serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        user_serializer = users.serializers.UserSerializer(data=request.data)

        if user_serializer.is_valid():
            user_serializer.save()

            return Response({
                'status': 'success',
                'user': user_serializer.data
            }, status=status.HTTP_201_CREATED)
        else:
            return Response({
                'status': 'error',
                'errors': user_serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, *args, **kwargs):
        user_serializer = users.serializers.UserSerializer(
            instance=request.user, data=request.data, partial=True)

        if user_serializer.is_valid():
            user_serializer.save()

            return Response({
                'status': 'success',
                'user': user_serializer.data
            }, status=status.HTTP_200_OK)
        else:
            return Response({
                'status': 'error',
                'errors': user_serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        self.request.user.delete()

        return Response({
            'status': 'success',
        }, status=status.HTTP_204_NO_CONTENT)
