from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, data):
        # check only on user creation
        if self.instance is None:
            if self.initial_data.get('repeat_password') is None:
                raise serializers.ValidationError({'repeat_password': 'This field is required.'})
            if data.get('password') != self.initial_data.get('repeat_password'):
                raise serializers.ValidationError({'repeat_password': 'Passwords do not match.'})
        return data

    def create(self, validated_data):
        validated_data['is_superuser'] = False
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        validated_data['is_superuser'] = False
        return super().update(instance, validated_data)
