#!/bin/bash

#./wait_for_db.sh

# check if PostgreSQL is ready to accept connections
while true
do
    pg_isready -h scheduler_db -p 5432 >/dev/null 2>&1
    RESULT=$?
    if [ ${RESULT} -eq 0 ]
    then
        echo "PostgreSQL db available."
        break
    else
        echo "PostgreSQL db is unavailable, sleeping 5 seconds..."
        sleep 5
    fi
done

yes | python manage.py collectstatic

python manage.py migrate

python manage.py runserver 0.0.0.0:8000