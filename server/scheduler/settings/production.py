"""
Settings for PRODUCTION environment
"""

from scheduler.settings.base import *

DEBUG = False
ALLOWED_HOSTS = ['*']