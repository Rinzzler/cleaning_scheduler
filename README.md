A sample project running a Django backend using PostgreSQL database and JavaScript frontend. 


To start backend, execute from project root:

	docker-compose up --build
To start frontend, execute from project root: 

	cd client
	npm install -g http-server
	http-server
